if [ $# -eq 0 ]
  then
    echo "No arguments supplied"
    exit 1
fi

echo Release version $1

cp ../target/simplebeangenerator-$1.jar .
cp ../target/simplebeangenerator-$1-javadoc.jar .
cp ../source.directory/simplebeangenerator-$1-sources.jar .
cp ../pom.xml .
rm -rf bundle.jar

gpg -ab pom.xml
gpg -ab simplebeangenerator-$1.jar
gpg -ab simplebeangenerator-$1-javadoc.jar
gpg -ab simplebeangenerator-$1-sources.jar

jar -cfv bundle.jar simplebeangenerator-$1.jar simplebeangenerator-$1.jar.asc simplebeangenerator-$1-javadoc.jar simplebeangenerator-$1-javadoc.jar.asc simplebeangenerator-$1-sources.jar simplebeangenerator-$1-sources.jar.asc pom.xml pom.xml.asc

