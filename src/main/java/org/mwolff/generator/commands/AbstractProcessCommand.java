package org.mwolff.generator.commands;

import org.mwolff.command.process.AbstractDefaultProcessCommand;
import org.mwolff.generator.config.CommandTransport;

public class AbstractProcessCommand extends AbstractDefaultProcessCommand<CommandTransport> {

    protected void addProcessFlow(CommandTransport context) {
        String result = context.getAsString("result");
        result += processID + " - ";
        context.put("result", result);
    }

    @Override
    public String executeAsProcess(CommandTransport context) {
        addProcessFlow(context);
        return null;
    }
}
