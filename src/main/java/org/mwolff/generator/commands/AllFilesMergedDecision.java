package org.mwolff.generator.commands;

import org.mwolff.command.interfaces.CommandTransition;
import org.mwolff.generator.config.CommandTransport;
import org.mwolff.toolbox.ValidTools;

import java.util.ArrayList;

import static org.mwolff.generator.commands.ProcessResults.REPEAT;
import static org.mwolff.generator.commands.ProcessResults.RESULT_OK;

public class AllFilesMergedDecision extends AbstractProcessCommand {

    private boolean initialized;

    public boolean isInitialized() {
        return initialized;
    }

    public void initialize(CommandTransport configuration) {

        if (isInitialized() || configuration.getBeanList() == null)
            return;

        configuration.setCopyBeanList(new ArrayList<>(configuration.getBeanList()));
        initialized = true;
    }

    @Override
    public String executeAsProcess(CommandTransport context) {

        super.executeAsProcess(context);
        initialize(context);

        if (!ValidTools.checkListIsValid(context.getCopyBeanList())) {
            return RESULT_OK;
        }

        getNextAndDelete(context);
        return REPEAT;
    }

    private void getNextAndDelete(CommandTransport context) {
        context.setTempBean(context.getCopyBeanList().get(0));
        context.getCopyBeanList().remove(0);
    }

    @Override
    public CommandTransition executeCommand(CommandTransport parameterObject) {
        return CommandTransition.SUCCESS;
    }

}
