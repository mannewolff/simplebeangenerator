/*
 Simple Bean Generator.

 Framework for easy building beans and data objects
 Download: https://gitlab.com/mannewolff/simplebeangenerator

 Copyright (C) 2018 - 2019 Manfred Wolff - Neusta Software Development

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package org.mwolff.generator.commands;

import org.mwolff.command.interfaces.CommandTransition;
import org.mwolff.generator.config.CommandTransport;
import org.mwolff.generator.config.ConfigReader;

import static org.mwolff.command.interfaces.CommandTransition.FAILURE;
import static org.mwolff.command.interfaces.CommandTransition.SUCCESS;
import static org.mwolff.generator.commands.ProcessResults.RESULT_ERROR;
import static org.mwolff.generator.commands.ProcessResults.RESULT_OK;

/**
 * Responsibility: Reads the configuration file and provide the properties
 * in a general <code>Configuration</code> object. For the process configuration
 * it is passed via the parameter object.
 *
 * @author Manfred Wolff
 * @since 1.0
 */
public class ConfigCommand extends AbstractProcessCommand {

    @Override
    public CommandTransition executeCommand(CommandTransport configuration) {
        return new ConfigReader().initialize(configuration) ? SUCCESS : FAILURE;
    }

    @Override
    public String executeAsProcess(CommandTransport context) {
        super.executeAsProcess(context);
        return executeCommand(context).equals(SUCCESS) ? RESULT_OK : RESULT_ERROR;
    }
}
