/*
 Simple Bean Generator.

 Framework for easy building beans and data objects
 Download: https://gitlab.com/mannewolff/simplebeangenerator

 Copyright (C) 2018 - 2019 Manfred Wolff - Neusta Software Development

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package org.mwolff.generator.commands;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.mwolff.command.interfaces.CommandTransition;
import org.mwolff.generator.config.CommandTransport;
import org.mwolff.generator.config.MergeTransport;

import java.io.File;
import java.io.PrintWriter;
import java.util.List;

import static org.mwolff.generator.commands.ProcessResults.RESULT_ERROR;
import static org.mwolff.generator.commands.ProcessResults.RESULT_OK;

/**
 * Writes a file for ONE artifact.
 * IN: configuration.fileToWrite
 * configuration.tempContent
 */
public class FileWriterCommand extends AbstractProcessCommand {

    private static final Logger LOG = LogManager.getLogger(FileWriterCommand.class);
    private MergeTransport transport;
    private boolean testmode;

    public void setTestmode(boolean testmode) {
        this.testmode = testmode;
    }

    @Override
    public CommandTransition executeCommand(CommandTransport configuration) {

        if (testmode) {

            File fileToWrite;
            fileToWrite = configuration.getFileToWrite();
            if (! printTransport(configuration, transport, fileToWrite)) return CommandTransition.FAILURE;

        } else {

            List<MergeTransport> transportlist = configuration.getTransports();

            for (MergeTransport t : transportlist) {

                String wholePath = configuration.getClasspath() + '/' + t.getTempOutputDir();
                new File(wholePath).mkdirs();
                String filePath = '/' + t.getTempOutputFile();
                File fileToWrite = new File(wholePath + filePath);

                if (! printTransport(configuration, t, fileToWrite)) return CommandTransition.FAILURE;
            }
        }
        return CommandTransition.SUCCESS;
    }

    private boolean printTransport(CommandTransport configuration, MergeTransport t, File fileToWrite) {

        try (PrintWriter out = new PrintWriter(fileToWrite)) {
            LOG.info("Writing file: " + fileToWrite.getAbsolutePath());
            LOG.info("Writing content: \n" + t.getTempContent());
            out.write(t.getTempContent());
        } catch (Exception e) {
            configuration.setLastError(String.format("File %s cannot been write.", fileToWrite));
            LOG.error(String.format(configuration.getLastError()));
            return false;
        }

        return true;
    }

    @Override
    public String executeAsProcess(CommandTransport context) {
        super.executeAsProcess(context);
        if (CommandTransition.SUCCESS.equals(executeCommand(context))) {
            return RESULT_OK;
        } else {
            return RESULT_ERROR;
        }
    }

    public void setMergeTransport(MergeTransport mergeTransport) {
        transport = mergeTransport;
    }
}