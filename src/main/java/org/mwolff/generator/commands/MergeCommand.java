/*
 Simple Bean Generator.

 Framework for easy building beans and data objects
 Download: https://gitlab.com/mannewolff/simplebeangenerator

 Copyright (C) 2018 - 2019 Manfred Wolff - Neusta Software Development

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package org.mwolff.generator.commands;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.tools.generic.DisplayTool;
import org.mwolff.command.interfaces.CommandTransition;
import org.mwolff.generator.config.CommandTransport;
import org.mwolff.generator.config.MergeTransport;
import org.mwolff.generator.xml.Bean;
import org.mwolff.toolbox.velocitytools.VelocityMerger;

import java.io.File;
import java.io.InputStream;
import java.util.Arrays;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

import static org.mwolff.generator.commands.ProcessResults.RESULT_OK;


/**
 * Responsibility: Wraps velocity things. Because something has to be done
 * because of the velocity framework, we've some comments in the file.
 *
 * @author Manfred Wolff
 * @since 1.0
 */
public class MergeCommand extends AbstractProcessCommand {

    private static final Logger LOG = LogManager.getLogger(MergeCommand.class);
    private MergeTransport transport;

    @Override
    public CommandTransition executeCommand(CommandTransport configuration) {

        final VelocityMerger velocityMerger = new VelocityMerger();
        velocityMerger.setTemplateFileName(decideTemplateFileName(configuration));
        VelocityContext veloContext = new VelocityContext();
        veloContext.put("display", new DisplayTool());

        Bean bean = configuration.getTempBean();
        LOG.info(bean.getXclass());

        transport = new MergeTransport();
        transport.setTempOutputDir(extractPackage(bean.getXclass()));
        proceedBean(configuration, velocityMerger, veloContext, bean);
        return CommandTransition.SUCCESS;
    }

    public String extractPackage(final String wholePath) {

        StringTokenizer tokenizer = new StringTokenizer(wholePath, "." );
        int i = tokenizer.countTokens();
        int j = 1;
        StringBuilder sb = new StringBuilder();
        while (j++ < i) {
            String token = tokenizer.nextToken();
            sb.append(token);
            sb.append(".");
        }
        return sb.toString().substring(0, sb.toString().length()-1);
    }

    /**
     * Velocity needs the file name of the template. If you use classpath strategy to load
     * that's enough. Otherwise we need the path. For that we've either to split the
     * path in to path and name or wie can just take the resource as classpath resource.
     */
    private String decideTemplateFileName(CommandTransport configuration) {

        if (!isClasspathTemplate(configuration)) {
            File file = new File(configuration.getClassTemplateName());
            return "/" + file.getName();
        } else
            return configuration.getClassTemplateName();
    }

    private boolean isClasspathTemplate(CommandTransport configuration) {
        LOG.info("Classpath Template Name:  " + configuration.getClassTemplateName());
        InputStream inputStream = this.getClass().getResourceAsStream(configuration.getClassTemplateName());
        if (inputStream != null)
            return true;
        else return false;
    }

    private String returnPath(final String input) {
        String[] split = input.split(File.separator);
        return Arrays.stream(split, 0, split.length - 1)
                .map(part -> "/" + part).collect(Collectors.joining());
    }

    private void proceedBean(CommandTransport configuration, VelocityMerger velocityMerger, VelocityContext veloContext, Bean bean) {

        LOG.info("Preparing: " + bean.getXclass());
        buildClassAndPackageName(bean, veloContext);

        LOG.info("Merging...   : " + bean.getXclass());
        if (isClasspathTemplate(configuration)) {
            LOG.info("isClasspathTemplate");
            transport.setTempContent(velocityMerger.mergeWithContext(veloContext, "", true));
        } else {
            LOG.info("is NOT ClasspathTemplate");
            String part = returnPath(configuration.getClassTemplateName());
            transport.setTempContent(velocityMerger.mergeWithContext(veloContext, part));
        }

        LOG.info("Writing   : " + bean.getXclass());
        String fileToWrite = bean.getXclass() + ".java";

        String packagePathToWrite = packageToPath((String) veloContext.get("packageString"));

        transport.setTempOutputDir(packagePathToWrite + '/');
        transport.setTempOutputFile(fileToWrite);
        configuration.addMergeTransport(transport);
    }

    private String packageToPath(final String path) {
        return path.replaceAll("\\.", "/");
    }

    private void buildClassAndPackageName(final Bean bean, final VelocityContext veloContext) {

        StringBuilder packagePath = new StringBuilder();

        int i = 0;
        String[] details = bean.getXclass().split("\\.");
        for (; i < details.length - 1; i++) packagePath.append(details[i]).append('.');
        bean.setXclass(details[i]);

        veloContext.put("packageString", packagePath.substring(0, packagePath.length() - 1));
        veloContext.put("bean", bean);
    }

    @Override
    public String executeAsProcess(CommandTransport context) {
        super.executeAsProcess(context);
        executeCommand(context);
        return RESULT_OK;
    }

    public MergeTransport getMergeTransport() {
        return transport;
    }
}