package org.mwolff.generator.commands;

public interface ProcessResults {

    static final String RESULT_OK = "OK";
    static final String RESULT_ERROR = "ERROR";
    static final String REPEAT = "REPEAT";

}
