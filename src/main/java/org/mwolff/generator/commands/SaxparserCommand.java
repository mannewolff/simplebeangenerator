/*
 Simple Bean Generator.

 Framework for easy building beans and data objects
 Download: https://gitlab.com/mannewolff/simplebeangenerator

 Copyright (C) 2018 - 2019 Manfred Wolff - Neusta Software Development

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package org.mwolff.generator.commands;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.mwolff.command.interfaces.CommandTransition;
import org.mwolff.generator.config.CommandTransport;
import org.mwolff.generator.xml.Bean;
import org.mwolff.generator.xml.BeanContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import javax.xml.XMLConstants;
import javax.xml.parsers.SAXParserFactory;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import static org.mwolff.command.interfaces.CommandTransition.FAILURE;
import static org.mwolff.command.interfaces.CommandTransition.SUCCESS;
import static org.mwolff.generator.commands.ProcessResults.RESULT_ERROR;
import static org.mwolff.generator.commands.ProcessResults.RESULT_OK;

/**
 * Parses the beans.xml which contains meta information for the generator.
 * Note: This class only parses one beans.xml file!
 */
public class SaxparserCommand extends AbstractProcessCommand {

    private static final Logger LOG = LogManager.getLogger(SaxparserCommand.class);

    @Override
    public CommandTransition executeCommand(CommandTransport configuration) {

        String resource = null;
        resource = configuration.getConfigPath() + "/" + configuration.getXmlFile();
        LOG.info(String.format("Parsing resource: %s", resource));
        try (FileInputStream fileInputStream = new FileInputStream(resource))
       {
            LOG.info(String.format("Reading resource: %s", resource));
            SAXParserFactory parserFactory = SAXParserFactory.newInstance();
            parserFactory.setNamespaceAware(true);
            final XMLReader xmlReader = parserFactory.newSAXParser().getXMLReader();
            xmlReader.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, ""); // Compliant
            xmlReader.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, ""); // compliant

            final BeanContentHandler handler = new BeanContentHandler();
            xmlReader.setContentHandler(handler);
            InputSource inputSource = new InputSource(fileInputStream);
            xmlReader.parse(inputSource);

            List<Bean> beans = handler.getBeans();
            configuration.setBeanList(beans);
        } catch (FileNotFoundException e) {
            String error = String.format("Resource %s is not found in the file system.", resource);
            LOG.error(configuration.setLastError(error));
            return FAILURE;

        } catch (Exception e) {
            LOG.error(configuration.setLastError(e.getMessage()));
            return FAILURE;
        }

        return SUCCESS;
    }

    @Override
    public String executeAsProcess(CommandTransport context) {
        super.executeAsProcess(context);
        if (SUCCESS.equals(executeCommand(context))) {
            return RESULT_OK;
        } else {
            return RESULT_ERROR;
        }
    }
}