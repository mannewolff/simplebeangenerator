package org.mwolff.generator.config;

import org.mwolff.generator.xml.Bean;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CommandTransport extends Configuration {

    List<Bean> beanList;        // List of beans, stored in a single file (not cleaned)
    List<Bean> copyBeanList;    // Copy of the List of beans, changed by AllFilesMergedDecision
    String properties;          // The generator.properties for this generator
    String tempDir;             // The temp directory to write rubbish
    File fileToWrite;           // For test purpose it is better to create file outside of an command!
    Bean   tempBean;            // temp string for transporting the bean to merge

    public List<Bean> getCopyBeanList() {
        return copyBeanList;
    }

    public void setCopyBeanList(List<Bean> copyBeanList) {
        this.copyBeanList = copyBeanList;
    }

    List<MergeTransport> transports = new ArrayList<>();

    public Bean getTempBean() {
        return tempBean;
    }

    public void setTempBean(Bean tempBean) {
        this.tempBean = tempBean;
    }

    public File getFileToWrite() {
        return fileToWrite;
    }

    public void setFileToWrite(File fileToWrite) {
        this.fileToWrite = fileToWrite;
    }

    public String getTempDir() {
        return tempDir;
    }

    public void setTempDir(String tempDir) {
        this.tempDir = tempDir;
    }

    public String getProperties() {
        return properties;
    }

    public void setProperties(String properties) {
        this.properties = properties;
    }

    public List<Bean> getBeanList() {
        return beanList;
    }

    public void setBeanList(List<Bean> beanList) {
        this.beanList = beanList;
    }

    public void addMergeTransport(MergeTransport transport) {
        transports.add(transport);
    }

    public List<MergeTransport> getTransports() {
        return new ArrayList<MergeTransport>(transports);
    }
}
