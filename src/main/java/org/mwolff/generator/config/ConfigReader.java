/*
 Simple Bean Generator.

 Framework for easy building beans and data objects
 Download: https://gitlab.com/mannewolff/simplebeangenerator

 Copyright (C) 2018 - 2019 Manfred Wolff - Neusta Software Development

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package org.mwolff.generator.config;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.mwolff.generator.config.resourceloader.PropertyResourceLoader;

import static org.mwolff.generator.config.resourceloader.PropertyResourceLoader.METHOD_FILESYSTEM;

public final class ConfigReader {

    private final PropertyResourceLoader loader = new PropertyResourceLoader();
    final Logger LOG = LogManager.getLogger(ConfigReader.class);

    public static final String GENERATOR_LANGUAGE      = "generator.language";
    public static final String GENERATOR_BASEPATH      = "generator.basepath";
    public static final String GENERATOR_CLASSPATH     = "generator.classpath";
    public static final String GENERATOR_METAINFO      = "generator.xmlmetainformation";
    public static final String GENERATOR_TEMPLATEPATH  = "generator.templatepath";
    public static final String GENERATOR_TEMPLATENAME  = "generator.templatename";
    public static final String GENERATOR_CONFIGPATH    = "generator.configpath";

    public boolean initialize(final CommandTransport configuration) {

        boolean result = loader.load(configuration.getProperties(), METHOD_FILESYSTEM);
        if (!result) {
            configuration.setLastError("Wrong resource: " + configuration.getProperties());
            return false;
        }

        configuration.setLanguage(loader.getProperty(GENERATOR_LANGUAGE));
        configuration.setBasepath(loader.getProperty(GENERATOR_BASEPATH));
        configuration.setClasspath(loader.getProperty(GENERATOR_CLASSPATH));
        configuration.setXmlFile(loader.getProperty(GENERATOR_METAINFO));
        configuration.setClassTemplateName(loader.getProperty(GENERATOR_TEMPLATENAME));
        configuration.setConfigPath(loader.getProperty(GENERATOR_CONFIGPATH));
        DefaultValueHander defaultValueHander = new DefaultValueHander();
        defaultValueHander.setDefaults(configuration);

        LOG.info("--------- Configuration of this generator ----------------");
        LOG.info("     Language is            : " + configuration.getLanguage());
        LOG.info("     Base path is           : " + configuration.getBasepath());
        LOG.info("     CLASSPATH is           : " + configuration.getClasspath());
        LOG.info("     XML meta file is       : " + configuration.getXmlFile());
        LOG.info("     Class template is is   : " + configuration.getClassTemplateName());
        LOG.info("     Config path is         : " + configuration.getConfigPath());
        LOG.info("-----------------------------------------------------------");

        return true;
    }
}
