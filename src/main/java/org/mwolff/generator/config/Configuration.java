/*
 Simple Bean Generator.

 Framework for easy building beans and data objects
 Download: https://gitlab.com/mannewolff/simplebeangenerator

 Copyright (C) 2018 - 2019 Manfred Wolff - Neusta Software Development

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package org.mwolff.generator.config;

import org.mwolff.command.parameterobject.DefaultParameterObject;

public class Configuration extends DefaultParameterObject {

    String language;            // The language to generate. Default is java.
    String basepath;            // The basepath of the generator
    String classpath;           // The classpath of the generator
    String xmlFile;             // The xml file with the meta information. Default is beans.xml
    String classTemplateName;   // The name of the classTemplate
    String configPath;          // The path of the configuration files

    String lastError;           // last computed error.


    public String getConfigPath() {
        return configPath;
    }

    public void setConfigPath(String configPath) {
        this.configPath = configPath;
    }


    public String getClassTemplateName() {
        return classTemplateName;
    }

    public void setClassTemplateName(String classTemplateName) {
        this.classTemplateName = classTemplateName;
    }

    public String getLastError() {
        return lastError;
    }

    public String setLastError(String lastError) {
        this.lastError = lastError;
        return lastError;
    }

    public String getXmlFile() {
        return xmlFile;
    }

    public void setXmlFile(String xmlFile) {
        this.xmlFile = xmlFile;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getBasepath() {
        return basepath;
    }

    public void setBasepath(String basepath) {
        this.basepath = basepath;
    }

    public String getClasspath() {
        return classpath;
    }

    public void setClasspath(String classpath) {
        this.classpath = classpath;
    }
}
