package org.mwolff.generator.config;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.mwolff.generator.config.resourceloader.PropertyResourceLoader;

public class DefaultValueHander {

    final Logger LOG = LogManager.getLogger(DefaultValueHander.class);

    public void setDefaults(CommandTransport configuration) {

        if ("".equals(configuration.getLanguage()))
            configuration.setLanguage(DefaultValues.generator_language);

        if ("".equals(configuration.getClassTemplateName()))
            configuration.setClassTemplateName(DefaultValues.generator_templatename);

        if ("".equals(configuration.getXmlFile()))
            configuration.setXmlFile(DefaultValues.generator_xmlmetainformation);

        if ("".equals(configuration.getClasspath())) {
            setClassPathDefault(configuration);
        }
    }

    private void setClassPathDefault(CommandTransport configuration) {
        String value = configuration.getClasspath();
        String classPath = DefaultValues.generator_classpath;
        if (classPath.contains("${generator.basepath}")) {

            PropertyResourceLoader configLoader = new PropertyResourceLoader();
            boolean result = configLoader.load(configuration.getProperties(), PropertyResourceLoader.METHOD_FILESYSTEM);

            if (!result) {
                configuration.setLastError(String.format("Resource %s is not found.", configuration.getProperties()));
                LOG.error(String.format("Resource %s is not found.", configuration.getProperties()));
                return;
            }

            String substitute = configLoader.getProperty(ConfigReader.GENERATOR_BASEPATH);
            classPath = classPath.replace("${generator.basepath}", substitute);
            configuration.setClasspath(classPath);
        }
    }
}
