package org.mwolff.generator.config;

public interface DefaultValues {

    /**
     * Default settings for the generator. They can be overwrite in the
     * generator properties
     */
    public static final String generator_language = "java";
    public static final String generator_classpath = "${generator.basepath}/gensrc/main/java";
    public static final String generator_xmlmetainformation = "beans.xml";
    public static final String generator_templatename = "/my-class-template.vm";
}
