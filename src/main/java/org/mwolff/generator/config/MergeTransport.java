package org.mwolff.generator.config;

/**
 * Result of the merging of tempate with the meta-model
 */
public class MergeTransport {

    String tempOutputFile;  // temp string for transporting the output file to write
    String tempOutputDir;   // temp string for transporting the output dir to write
    String tempContent;     // temp string for transporting the content of the file to write

    public String getTempOutputFile() {
        return tempOutputFile;
    }

    public void setTempOutputFile(String tempOutputFile) {
        this.tempOutputFile = tempOutputFile;
    }

    public String getTempOutputDir() {
        return tempOutputDir;
    }

    public void setTempOutputDir(String tempOutputDir) {
        this.tempOutputDir = tempOutputDir;
    }

    public String getTempContent() {
        return tempContent;
    }

    public void setTempContent(String tempContent) {
        this.tempContent = tempContent;
    }
}
