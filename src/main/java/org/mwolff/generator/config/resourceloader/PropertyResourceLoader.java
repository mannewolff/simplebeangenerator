/*
 Simple Bean Generator.

 Framework for easy building beans and data objects
 Download: https://gitlab.com/mannewolff/simplebeangenerator

 Copyright (C) 2018 - 2019 Manfred Wolff - Neusta Software Development

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package org.mwolff.generator.config.resourceloader;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Pattern;

public final class PropertyResourceLoader {

    private static final Logger LOG = LogManager.getLogger(PropertyResourceLoader.class);

    private InputStream inputStream;
    private FileInputStream fileInputStream;

    private final Properties properties = new Properties();

    public static final String METHOD_CLASSPATH  = "method.classpath";
    public static final String METHOD_FILESYSTEM = "method.filesystem";

    public boolean load(String resource, String method) {

        if (method.equals(METHOD_CLASSPATH)) {
            LOG.info("Reading inputstream from classpath /" + resource);
            inputStream = this.getClass().getResourceAsStream("/" + resource);
        }

        if (method.equals(METHOD_FILESYSTEM)) {
            try {
                LOG.info("Reading inputstream from file system " + resource);
                fileInputStream = new FileInputStream(resource);
                inputStream = fileInputStream;
            } catch (FileNotFoundException e) {
                return false;
            }
        }

        if (inputStream == null) {
            return false;
        }

        getResourceAsPropertyFile(inputStream);
        return true;
    }

    public Properties getProperties() {
        return (Properties) properties.clone();
    }


    public void getResourceAsPropertyFile(InputStream inputStream) {
        try {
            properties.load(inputStream);
        } catch (Exception e) {
            // more or less not possible, that this happened
        }
    }

    public String getProperty(String property) {
        String resultString;
        resultString = properties.getProperty(property);
        if (resultString == null) {
            return "";
        }
        final Set<Object> keySet = properties.keySet();
        for (final Object object : keySet) {
            final String key = (String) object;
            if (resultString.contains("${" + key + "}")) {
                final String value = properties.getProperty(key);
                resultString = resultString.replaceAll(Pattern.quote("${" + key + "}"), value);
            }
        }
        return resultString;
    }

    public boolean fillBean(Object testBean) {

        boolean result = true;
        final Set<Object> keySet = properties.keySet();
        for (final Object object : keySet) {
            final String key = (String) object;
            final String value = this.getProperty(key);
            try {
                PropertyUtils.setSimpleProperty(testBean, key, value);
            } catch (Exception e) {
                LOG.info(String.format("Property not found %s.", key));
                result = false;
            }
        }
        return result;
    }
}