/*
 Simple Bean Generator.

 Framework for easy building beans and data objects
 Download: https://gitlab.com/mannewolff/simplebeangenerator

 Copyright (C) 2018 - 2019 Manfred Wolff - Neusta Software Development

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package org.mwolff.generator.xml;

import java.util.ArrayList;
import java.util.List;

public final class Bean {

    private String xclass;
    private String xextends;

    private final List<Property> properties = new ArrayList<>();
    private final List<Import> imports = new ArrayList<>();

    public void addProperty(final Property property) {
        properties.add(property);
    }

    public void addImport(final Import ximport) {
        imports.add(ximport);
    }

    public List<Property> getProperties() {
        return new ArrayList<>(properties);
    }

    public List<Import> getImports() {
        return new ArrayList<>(imports);
    }

    public String getXclass() {
        return xclass;
    }

    public void setXclass(String xclass) {
        this.xclass = xclass;
    }

    public String getXextends() {
        return xextends;
    }

    public void setXextends(String xextends) {
        this.xextends = xextends;
    }

}

