/*
 Simple Bean Generator.

 Framework for easy building beans and data objects
 Download: https://gitlab.com/mannewolff/simplebeangenerator

 Copyright (C) 2018 - 2019 Manfred Wolff - Neusta Software Development

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package org.mwolff.generator.xml;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * SAX content handler to handle parsing beans.
 *
 * @author mwolff
 */
public final class BeanContentHandler extends DefaultHandler {

    private final List<Bean> beans = new ArrayList<>();

    private Bean bean;

    public List<Bean> getBeans() {
        return beans;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes atts) {

        Property property;

        if ("import".equals(localName)) {
            Import ximport = new Import();
            ximport.setName(atts.getValue("name"));
            if (atts.getValue("static").equals("true")) {
                ximport.setXstatic(true);
            } else {
                ximport.setXstatic(false);
            }
            bean.addImport(ximport);
        }

        if ("bean".equals(localName)) {
            bean = new Bean();
            bean.setXclass(atts.getValue("class"));
            bean.setXextends(atts.getValue("extends"));
        }

        if ("property".equals(localName)) {
            property = new Property();
            property.setName(atts.getValue("name"));
            property.setType(atts.getValue("type"));
            bean.addProperty(property);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {

        if ("bean".equals(localName)) {
            beans.add(bean);
        }
    }

}