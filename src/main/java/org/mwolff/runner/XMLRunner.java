package org.mwolff.runner;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.mwolff.command.builder.XMLChainBuilder;
import org.mwolff.generator.config.CommandTransport;
import org.mwolff.generator.config.Configuration;

public class XMLRunner {

    private static final Logger LOG = LogManager.getLogger(XMLRunner.class);

    public static void main(String[] args) {

        LOG.info("Paramter: " + args[0]);
        CommandTransport configuration = new CommandTransport();
        configuration.setProperties(args[0]);

        XMLChainBuilder<Configuration> builder = new XMLChainBuilder<>("/generator-chain.xml");
        builder.executeAsProcess("START", configuration);
    }
}


