package org.mwolff.test;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.mwolff.generator.config.CommandTransport;
import org.mwolff.generator.config.resourceloader.PropertyResourceLoader;

/**
 * Getting the defaults for tests and put them into a Configuration Object
 */
public class ConfigurationFactory {

    final static Logger LOG = LogManager.getLogger(ConfigurationFactory.class);

    public static CommandTransport getInstance() {
        CommandTransport commandTransport = new CommandTransport();
        ConfigurationFactory.setDefaults(commandTransport);
        return commandTransport;
    }

    public static void setDefaults(CommandTransport commandTransport) {

        LOG.info("Configuration Factory");

        PropertyResourceLoader propertyResourceLoader = new PropertyResourceLoader();
        propertyResourceLoader.load("test.configuration.properties", PropertyResourceLoader.METHOD_CLASSPATH);

        commandTransport.setBasepath(propertyResourceLoader.getProperty("generator.basepath"));
        LOG.info("Base path is    : " + commandTransport.getBasepath());
        commandTransport.setClasspath(propertyResourceLoader.getProperty("generator.classpath"));
        LOG.info("CLASSPATH is    : " + commandTransport.getClasspath());
        commandTransport.setConfigPath(propertyResourceLoader.getProperty("generator.configpath"));
        LOG.info("Config path is  : " + commandTransport.getConfigPath());
        commandTransport.setTempDir(propertyResourceLoader.getProperty("test.path"));
        LOG.info("Temp dir is     : " + commandTransport.getTempDir());

        commandTransport.setProperties(propertyResourceLoader.getProperty("generator.property"));
        LOG.info("Property is     : " + commandTransport.getProperties());
    }


}
