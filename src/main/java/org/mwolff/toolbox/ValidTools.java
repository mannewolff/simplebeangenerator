package org.mwolff.toolbox;

import java.util.List;

public class ValidTools {

    //  List<? extends Object>

    public static  boolean checkListIsValid(List<? extends Object> listToCheck) {
        return listToCheck != null && ! listToCheck.isEmpty();
    }

}
