/*
 Simple Bean Generator.

 Framework for easy building beans and data objects
 Download: https://gitlab.com/mannewolff/simplebeangenerator

 Copyright (C) 2018 - 2019 Manfred Wolff - Neusta Software Development

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package org.mwolff.toolbox.velocitytools;

/**
 * Exception which works with this framework.
 */
@SuppressWarnings("serial")
public class VelocityException extends Exception {

    public VelocityException() {
        super();
    }

    public VelocityException(final String message) {
        super(message);
    }

    public VelocityException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public VelocityException(final Throwable cause) {
        super(cause);
    }
}