/*
 Simple Bean Generator.

 Framework for easy building beans and data objects
 Download: https://gitlab.com/mannewolff/simplebeangenerator

 Copyright (C) 2018 - 2019 Manfred Wolff - Neusta Software Development

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package org.mwolff.toolbox.velocitytools;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import java.io.StringWriter;
import java.util.Properties;

/**
 * Merges a velocity template with properties of the property file.
 */
public class VelocityMerger {

    private String template;
    private Properties properties;

    public void setTemplateFileName(final String template) {
        this.template = template;
    }

    public void setProperties(final Properties properties) {
        this.properties = properties;
    }

    public String mergeWithPropertyFileWithFilePath(final String pathToTemplate) {
        final VelocityEngine velocityEngine = initializeVelocityEngineWithFilePath(pathToTemplate);
        final VelocityContext context = new VelocityContext();
        movePropertiesToContext(context);
        final StringWriter writer = mergeTemplateWithContext(velocityEngine, context);
        return writer.toString();
    }

    public String mergeWithPropertyWithClassPath() {
        final VelocityEngine velocityEngine = initializeVelocityEngineWithClassPath();
        final VelocityContext context = new VelocityContext();
        movePropertiesToContext(context);
        final StringWriter writer = mergeTemplateWithContext(velocityEngine, context);
        return writer.toString();
    }

    private VelocityEngine initializeVelocityEngineWithFilePath(final String pathToTemplate) {
        final VelocityEngine ve = new VelocityEngine();
        ve.setProperty(RuntimeConstants.FILE_RESOURCE_LOADER_PATH, pathToTemplate);
        return ve;
    }

    private VelocityEngine initializeVelocityEngineWithClassPath() {
        final VelocityEngine ve = new VelocityEngine();
        ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        return ve;
    }

    private StringWriter mergeTemplateWithContext(final VelocityEngine velocityEngine, final VelocityContext context) {
        Template veloTemplate;
        veloTemplate = velocityEngine.getTemplate(template);
        final StringWriter writer = new StringWriter();
        veloTemplate.merge(context, writer);
        return writer;
    }

    private void movePropertiesToContext(final VelocityContext context) {
        for (final String prop : properties.stringPropertyNames()) {
            final String value = properties.getProperty(prop);
            context.put(prop, value);
        }
    }

    /**
     * Only for test purposes to test all the fragments.
     */
    public static void evaluate(VelocityContext context, StringWriter w, String s) {
        Velocity.evaluate(context, w, "mystring", s);
    }

    public String mergeWithContext(final VelocityContext context, final String pathToTemplate) {
        final VelocityEngine velocityEngine = initializeVelocityEngineWithFilePath(pathToTemplate);
        final StringWriter writer = mergeTemplateWithContext(velocityEngine, context);
        return writer.toString();
    }

    public String mergeWithContext(final VelocityContext context, final String pathToTemplate, boolean cp) {
        final VelocityEngine velocityEngine = initializeVelocityEngineWithClassPath();
        final StringWriter writer = mergeTemplateWithContext(velocityEngine, context);
        return writer.toString();
    }
}