package org.mwolff.generator.commands;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.mwolff.generator.config.CommandTransport;
import org.mwolff.generator.xml.Bean;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mwolff.generator.commands.ProcessResults.REPEAT;
import static org.mwolff.generator.commands.ProcessResults.RESULT_OK;

class AllFilesMergedDecisionTest {

    CommandTransport configuration;

    @Test
    void executeAsProcessReturnsRepeat() {

        // given
        Bean bean = new Bean();
        List<Bean> beanList = new ArrayList<>();
        beanList.add(bean);
        AllFilesMergedDecision allFilesMergedDecision = new AllFilesMergedDecision();

        // when
        CommandTransport configuration = new CommandTransport();
        configuration.setBeanList(beanList);
        String result = allFilesMergedDecision.executeAsProcess(configuration);

        // then
        assertThat(allFilesMergedDecision.isInitialized(), is(Boolean.TRUE));
        assertThat(result, is(REPEAT));
    }

    @Test
    void executeAsProcessWithNullBean() {

        // given
        AllFilesMergedDecision allFilesMergedDecision = new AllFilesMergedDecision();

        // when
        CommandTransport configuration = new CommandTransport();
        configuration.setBeanList(null);
        String result = allFilesMergedDecision.executeAsProcess(configuration);

        // then
        assertThat(allFilesMergedDecision.isInitialized(), is(Boolean.FALSE));
        assertThat(result, is(RESULT_OK));
    }

    @Test
    void executeAsProcessWithEmptyBean() {

        // given
        AllFilesMergedDecision allFilesMergedDecision = new AllFilesMergedDecision();

        // when
        CommandTransport configuration = new CommandTransport();
        List<Bean> beanList = new ArrayList<>();
        configuration.setBeanList(beanList);

        String result = allFilesMergedDecision.executeAsProcess(configuration);

        // then
        assertThat(allFilesMergedDecision.isInitialized(), is(Boolean.TRUE));
        assertThat(result, is(RESULT_OK));
    }

    @Test
    void initializer() {

        // given
        List<Bean> beanList = new ArrayList<>();
        Bean bean = new Bean();
        bean.setXclass("Testclass");
        beanList.add(bean);

        CommandTransport configuration = new CommandTransport();
        configuration.setBeanList(beanList);
        AllFilesMergedDecision allFilesMergedDecision = new AllFilesMergedDecision();

        // when
        String result = allFilesMergedDecision.executeAsProcess(configuration);

        // then
        assertThat(allFilesMergedDecision.isInitialized(), is(Boolean.TRUE));
        assertThat(result, is(REPEAT));
    }

    @Test
    void initializerFailBecauseBeanNull() {
        CommandTransport configuration = new CommandTransport();
        configuration.setBeanList(null);
        AllFilesMergedDecision allFilesMergedDecision = new AllFilesMergedDecision();
        assertThat(allFilesMergedDecision.isInitialized(), is(Boolean.FALSE));
    }

    @Test
    void initializerFailBecausItIsInitialized() {
        // given
        List<Bean> beanList = new ArrayList<>();
        Bean bean = new Bean();
        bean.setXclass("Testclass");
        beanList.add(bean);

        CommandTransport configuration = new CommandTransport();
        configuration.setBeanList(beanList);
        AllFilesMergedDecision allFilesMergedDecision = new AllFilesMergedDecision();

        // when
        String result = allFilesMergedDecision.executeAsProcess(configuration);

        // then
        assertThat(allFilesMergedDecision.isInitialized(), is(Boolean.TRUE));

        allFilesMergedDecision.initialize(configuration);
        assertThat(allFilesMergedDecision.isInitialized(), is(Boolean.TRUE));


    }

    @Test
    void setNextBeanInTempBean() {

        // given
        List<Bean> beanList = new ArrayList<>();
        Bean bean = new Bean();
        bean.setXclass("Testclass");
        beanList.add(bean);
        bean = new Bean();
        bean.setXclass("Tempclass");
        beanList.add(bean);

        CommandTransport configuration = new CommandTransport();
        configuration.setBeanList(beanList);
        AllFilesMergedDecision allFilesMergedDecision = new AllFilesMergedDecision();

        // when
        String result = allFilesMergedDecision.executeAsProcess(configuration);

        // then
        assertThat(result, is(REPEAT));
        Bean nextBean = configuration.getTempBean();
        assertThat(nextBean, CoreMatchers.notNullValue());
        assertThat(nextBean.getXclass(), is("Testclass"));
        assertThat(configuration.getCopyBeanList().size(), is(1));
        assertThat(configuration.getCopyBeanList().get(0).getXclass(), is("Tempclass"));

    }
}