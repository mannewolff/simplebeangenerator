/*
 Simple Bean Generator.

 Framework for easy building beans and data objects
 Download: https://gitlab.com/mannewolff/simplebeangenerator

 Copyright (C) 2018 - 2019 Manfred Wolff - Neusta Software Development

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package org.mwolff.generator.commands;

import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.mwolff.generator.config.CommandTransport;
import org.mwolff.test.ConfigurationFactory;

import java.nio.file.Files;
import java.nio.file.Paths;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ConfigCommandTest {

    @Test
    public void configurationOK() {

        CommandTransport configuration = ConfigurationFactory.getInstance();

        ConfigCommand configCommand = new ConfigCommand();
        configCommand.setProcessID("ID");
        String result = configCommand.executeAsProcess(configuration);
        assertThat(result, is("OK"));
        assertThat(configuration.getLanguage(), CoreMatchers.notNullValue());
        assertThat(Files.exists(Paths.get(configuration.getBasepath())), is(Boolean.TRUE));
        assertThat(Files.exists(Paths.get(configuration.getClasspath())), is(Boolean.TRUE));
        assertThat(Files.exists(Paths.get("/not/a/really/path")), is(Boolean.FALSE));
        String strresult = configuration.getAsString("result");
        assertThat(strresult, is("ID - "));
    }

    @Test
    public void configurationNotOK() {
        ConfigCommand configCommand = new ConfigCommand();
        CommandTransport configuration = new CommandTransport();
        configuration.setProperties("wrong.thing");
        String result = configCommand.executeAsProcess(configuration);
        assertThat(result, is("ERROR"));
        assertThat(configuration.getLastError(), is("Wrong resource: wrong.thing"));
    }
}