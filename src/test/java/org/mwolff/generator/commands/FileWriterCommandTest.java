/*
 Simple Bean Generator.

 Framework for easy building beans and data objects
 Download: https://gitlab.com/mannewolff/simplebeangenerator

 Copyright (C) 2018 - 2019 Manfred Wolff - Neusta Software Development

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package org.mwolff.generator.commands;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mwolff.generator.config.CommandTransport;
import org.mwolff.generator.config.MergeTransport;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


public class FileWriterCommandTest {


    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    CommandTransport configuration;

    @Test
    public void writeFileOKinTestmode() throws Exception {

        // given
        final MergeTransport mergeTransport = new MergeTransport();
        final CommandTransport configuration = new CommandTransport();
        mergeTransport.setTempOutputFile("xxx.java");
        mergeTransport.setTempContent("package org.mwolff;");
        File file = folder.newFile(mergeTransport.getTempOutputFile());
        configuration.setFileToWrite(file);
        FileWriterCommand fileWriterCommand = new FileWriterCommand();
        fileWriterCommand.setTestmode(true);
        fileWriterCommand.setMergeTransport(mergeTransport);

        fileWriterCommand.setProcessID("ID");

        // when
        String result = fileWriterCommand.executeAsProcess(configuration);
        assertThat(result, is("OK"));

        // then
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        String zeile1 = bufferedReader.readLine();
        assertThat(zeile1, is("package org.mwolff;"));
        bufferedReader.close();

        String strresult = configuration.getAsString("result");
        assertThat(strresult, is("ID - "));
    }

    @Test
    public void writeFileOKinOrigianlmode() throws Exception {

        // given
        final MergeTransport mergeTransport = new MergeTransport();
        final CommandTransport configuration = new CommandTransport();
        configuration.setClasspath("target");
        mergeTransport.setTempOutputDir("gensrc/org/mwolff");
        mergeTransport.setTempOutputFile("xxx.java");
        mergeTransport.setTempContent("package org.mwolff;");
        configuration.setFileToWrite(null);
        configuration.addMergeTransport(mergeTransport);

        FileWriterCommand fileWriterCommand = new FileWriterCommand();
        fileWriterCommand.setTestmode(false);

        fileWriterCommand.setProcessID("ID");

        // when
        String result = fileWriterCommand.executeAsProcess(configuration);
        assertThat(result, is("OK"));

        // then

        File file = new File(configuration.getClasspath() + '/' + mergeTransport.getTempOutputDir() +
                '/' + mergeTransport.getTempOutputFile());
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        String zeile1 = bufferedReader.readLine();
        assertThat(zeile1, is("package org.mwolff;"));
        bufferedReader.close();

        String strresult = configuration.getAsString("result");
        assertThat(strresult, is("ID - "));
    }

    @Test
    public void writeFileNotOKTest() throws Exception {

        // given
        final MergeTransport mergeTransport = new MergeTransport();
        final CommandTransport configuration = new CommandTransport();
        mergeTransport.setTempOutputFile("xxx.java");
        mergeTransport.setTempContent("package org.mwolff;");
        configuration.setFileToWrite(null);
        FileWriterCommand fileWriterCommand = new FileWriterCommand();
        fileWriterCommand.setMergeTransport(mergeTransport);
        fileWriterCommand.setTestmode(true);

        // when
        String result = fileWriterCommand.executeAsProcess(configuration);
        assertThat(result, is("ERROR"));
        assertThat(configuration.getLastError(), is("File null cannot been write."));
    }
}