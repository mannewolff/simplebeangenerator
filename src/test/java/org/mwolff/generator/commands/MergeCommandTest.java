/*
 Simple Bean Generator.

 Framework for easy building beans and data objects
 Download: https://gitlab.com/mannewolff/simplebeangenerator

 Copyright (C) 2018 - 2019 Manfred Wolff - Neusta Software Development

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package org.mwolff.generator.commands;

import org.junit.Test;
import org.mwolff.generator.config.CommandTransport;
import org.mwolff.generator.config.MergeTransport;
import org.mwolff.generator.xml.Bean;
import org.mwolff.test.ConfigurationFactory;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class MergeCommandTest {

    @Test
    public void testOKWithOriginalClassTemplate() {

        // given
        final CommandTransport configuration = new CommandTransport();

        configuration.setClassTemplateName("/my-class-template.vm");

        final Bean bean = new Bean();
        bean.setXclass("org.mwolff.data.AbstractData");
        configuration.setTempBean(bean);

        MergeCommand mergeCommand = new MergeCommand();
        //mergeCommand.setMergeTransport(mergeTransport);

        mergeCommand.setProcessID("mergeID");
        String result = mergeCommand.executeAsProcess(configuration);
        assertThat(result, is("OK"));

        String content = mergeCommand.getMergeTransport().getTempContent();
        String[] lines = content.split("\n");
        assertThat(lines[0], is("package org.mwolff.data;"));

        String output = mergeCommand.getMergeTransport().getTempOutputFile();
        assertThat(output, is("AbstractData.java"));

        String strresult = configuration.getAsString("result");
        assertThat(strresult, is("mergeID - "));

        List<MergeTransport> mergeTransportList = configuration.getTransports();
        assertThat(mergeTransportList.size(), is(1));
    }

    @Test
    public void testOKWithModifiedTemplate() {

        CommandTransport configuration = ConfigurationFactory.getInstance();

        configuration.setClasspath(configuration.getTempDir());
        configuration.setXmlFile("beans.xml");
        configuration.setClassTemplateName(configuration.getBasepath() + "/src/test/resources/class-template-modified.vm");

        SaxparserCommand saxparserCommand = new SaxparserCommand();
        saxparserCommand.executeAsProcess(configuration);
        configuration.setTempBean(configuration.getBeanList().get(0));

        MergeCommand mergeCommand = new MergeCommand();
        //mergeCommand.setMergeTransport(mergeTransport);
        String result = mergeCommand.executeAsProcess(configuration);
        assertThat(result, is("OK"));

        String content = mergeCommand.getMergeTransport().getTempContent();
        String[] lines = content.split("\n");
        assertThat(lines[0], is("package org.mwolff.data; // Modified Template"));

        String output = mergeCommand.getMergeTransport().getTempOutputFile();
        assertThat(output, is("AbstractData.java"));
    }

    @Test
    public void testExtractPackage() {
        // given
        MergeCommand mergeCommand = new MergeCommand();
        String path = "org.mwolff.data.AbstractData";

        // when
        String result = mergeCommand.extractPackage(path);
        System.out.println(result);

        // than
        assertThat(result, is("org.mwolff.data"));
    }
}

