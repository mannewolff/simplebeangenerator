/*
 Simple Bean Generator.

 Framework for easy building beans and data objects
 Download: https://gitlab.com/mannewolff/simplebeangenerator

 Copyright (C) 2018 - 2019 Manfred Wolff - Neusta Software Development

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package org.mwolff.generator.commands;

import org.junit.Before;
import org.junit.Test;
import org.mwolff.generator.config.CommandTransport;
import org.mwolff.generator.xml.Bean;
import org.mwolff.test.ConfigurationFactory;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mwolff.generator.commands.ProcessResults.RESULT_ERROR;
import static org.mwolff.generator.commands.ProcessResults.RESULT_OK;

public class SaxparserCommandTest {

    CommandTransport configuration;

    @Before
    public void setUp() {
        configuration = ConfigurationFactory.getInstance();
    }

    @Test
    public void wrongXMLFileName() {
        String lasterror = "Resource "+ configuration.getBasepath()+"/src/main/resources/badPath " +
                           "is not found in the file system.";
        configuration.setXmlFile("badPath");
        assertThat(new SaxparserCommand().executeAsProcess(configuration), is(RESULT_ERROR));
        assertThat(configuration.getLastError(), is(lasterror));
    }

    @Test
    public void wrongXMLFormat() {
        configuration.setXmlFile("wrongbeans.xml");
        assertThat(new SaxparserCommand().executeAsProcess(configuration), is(RESULT_ERROR));
        assertThat(configuration.getLastError(), is("Content is not allowed in prolog."));
    }

    @Test
    public void validXMLFile() {
        configuration.setXmlFile("beans.xml");
        SaxparserCommand saxparserCommand = new SaxparserCommand();
        saxparserCommand.setProcessID("ID");
        assertThat(saxparserCommand.executeAsProcess(configuration), is(RESULT_OK));

        List<Bean> beanList = configuration.getBeanList();
        assertThat(beanList.size(), is(3));

        String strresult = configuration.getAsString("result");
        assertThat(strresult, is("ID - "));

    }
}