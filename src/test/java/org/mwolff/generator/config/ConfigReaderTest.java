/*
 Simple Bean Generator.

 Framework for easy building beans and data objects
 Download: https://gitlab.com/mannewolff/simplebeangenerator

 Copyright (C) 2018 - 2019 Manfred Wolff - Neusta Software Development

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package org.mwolff.generator.config;

import org.junit.Test;
import org.mwolff.test.ConfigurationFactory;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ConfigReaderTest {

    @Test
    public void testWrongResource() {
        ConfigReader reader = new ConfigReader();
        CommandTransport configuration = ConfigurationFactory.getInstance();
        configuration.setProperties("wrong.properties");
        boolean result = reader.initialize(configuration);
        assertThat((result), is(false));

    }

    @Test
    public void testInitializing() throws Exception {

        ConfigReader reader = new ConfigReader();
        CommandTransport configuration = ConfigurationFactory.getInstance();
        boolean result = reader.initialize(configuration);
        assertThat((result), is(true));

        assertThat(configuration.getLanguage(), containsString("java"));
        assertThat(configuration.getBasepath(), containsString("simplebeangenerator"));
        assertThat(configuration.getClasspath(), containsString("simplebeangenerator/gensrc/main/java"));
        assertThat(configuration.getXmlFile(), is("beans.xml"));
        assertThat(configuration.getClassTemplateName(), is("/my-class-template.vm"));
        assertThat(configuration.getConfigPath(), containsString("simplebeangenerator/src/main/resources"));
    }
}