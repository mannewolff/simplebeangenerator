package org.mwolff.generator.config;

import org.junit.Before;
import org.junit.Test;
import org.mwolff.test.ConfigurationFactory;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class DefaultValueHanderTest {

    private DefaultValueHander defaultValueHander;
    private CommandTransport configuration;

    @Before
    public void setUp() {
        configuration = ConfigurationFactory.getInstance();
        defaultValueHander = new DefaultValueHander();
    }

    @Test
    public void defaultJava() {

        configuration.setLanguage("");
        defaultValueHander.setDefaults(configuration);

        assertThat(configuration.getLanguage(), is("java"));
    }

    @Test
    public void defaultGeneratorClassPath() {

        configuration.setClasspath("");
        defaultValueHander.setDefaults(configuration);

        assertThat(configuration.getClasspath(), containsString("simplebeangenerator/gensrc/main/java"));
    }

    @Test
    public void generatorClassPathWithErrors() {
        configuration.setClasspath("");
        configuration.setProperties("unknown");
        defaultValueHander.setDefaults(configuration);

        assertThat(configuration.getClasspath(), is(""));
        assertThat(configuration.getLastError(), is("Resource unknown is not found."));

    }

    @Test
    public void defaultXmlMetaInformation() {
        configuration.setXmlFile("");
        defaultValueHander.setDefaults(configuration);

        assertThat(configuration.getXmlFile(), is("beans.xml"));
    }

    @Test
    public void defaultTemplateName() {
        configuration.setClassTemplateName("");
        defaultValueHander.setDefaults(configuration);

        assertThat(configuration.getClassTemplateName(), is("/my-class-template.vm"));
    }
}