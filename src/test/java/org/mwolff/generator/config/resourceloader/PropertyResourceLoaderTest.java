/*
 Simple Bean Generator.

 Framework for easy building beans and data objects
 Download: https://gitlab.com/mannewolff/simplebeangenerator

 Copyright (C) 2018 - 2019 Manfred Wolff - Neusta Software Development

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package org.mwolff.generator.config.resourceloader;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mwolff.generator.config.resourceloader.PropertyResourceLoader.METHOD_CLASSPATH;
import static org.mwolff.generator.config.resourceloader.PropertyResourceLoader.METHOD_FILESYSTEM;

public class PropertyResourceLoaderTest {

    private static final Logger LOG = LogManager.getLogger(PropertyResourceLoaderTest.class);

    private final String classPathRessource = "config.properties";
    PropertyResourceLoader propertyResourceLoader;

    @Before
    public void setUp() {
        LOG.info("Set up Log4J");
        propertyResourceLoader = new PropertyResourceLoader();
    }

    @Test
    public void testReadBadFile() throws Exception {
        final boolean result = propertyResourceLoader.load("badfile", METHOD_CLASSPATH);
        assertThat(result, is(false));
    }

    /**
     * A test for things that never can happened. But the framework throws an
     * Exception so we have to got this long path.
     */
    @Test
    public void testLoadPropertiesWithWronInputStream() throws Exception{
        propertyResourceLoader.getResourceAsPropertyFile(null);
    }

    @Test
    public void testReadGoodFile() throws Exception {
        final boolean result = propertyResourceLoader.load("src/main/resources/generator.properties", METHOD_FILESYSTEM);
        assertThat(result, is(true));
    }

    public void testReadFilePerFilesystem() {
        final boolean result = propertyResourceLoader.load(classPathRessource, METHOD_CLASSPATH);
        assertThat(result, is(true));
    }

    @Test
    public void testIsNullPropertyFile() throws Exception {
        final boolean result = propertyResourceLoader.load("badfile", METHOD_CLASSPATH);
        assertThat(propertyResourceLoader.getProperties().size(), is(0));
        assertThat(result, is(false));
    }

    @Test
    public void testIsPropertyFile() throws Exception {
        final boolean result = propertyResourceLoader.load(classPathRessource, METHOD_CLASSPATH);
        assertThat(propertyResourceLoader.getProperties(), notNullValue());
        assertThat(result, is(true));
    }

    @Test
    public void testSimpleProperty() throws Exception {
        final boolean result = propertyResourceLoader.load(classPathRessource, METHOD_CLASSPATH);
        String value = propertyResourceLoader.getProperty("hello");
        assertThat(value, is("hello"));
        assertThat(result, is(true));
    }

    /**
     * Because there might be defaultvalues for some keys it maybe is a
     * good thing, that a missing key returns "", so you can set the
     * return value. It might not be an error if a key is missing
     */
    @Test
    public void testNullProperty() throws Exception {
        final boolean result = propertyResourceLoader.load(classPathRessource, METHOD_CLASSPATH);
        String value = propertyResourceLoader.getProperty("notThere");
        assertThat(value, is(""));
        assertThat(result, is(true));
    }

    /**
     * hello=hello
     * world=world
     * HelloWorld=${hello}${world}
     */
    @Test
    public void testSubstitution() throws Exception {
        final boolean result = propertyResourceLoader.load(classPathRessource, METHOD_CLASSPATH);
        String value = propertyResourceLoader.getProperty("helloWorld");
        assertThat(value, is("helloworld"));
        assertThat(result, is(true));
    }

    @Test
    public void testBeanFillUpWithErrors() throws Exception {
        final boolean result = propertyResourceLoader.load(classPathRessource, METHOD_CLASSPATH);
        TestBean testBean = new TestBean();
        boolean methodResult = propertyResourceLoader.fillBean(testBean);
        assertThat(testBean.getHello(), is("hello"));
        assertThat(testBean.getWorld(), is("world"));
        assertThat(methodResult, is(false));
        assertThat(result, is(true));
    }

    @Test
    public void testBeanFillUpWithputErrors() throws Exception {
        final boolean result = propertyResourceLoader.load(classPathRessource, METHOD_CLASSPATH);
        TestBeanFull testBean = new TestBeanFull();
        boolean methodResult = propertyResourceLoader.fillBean(testBean);
        assertThat(testBean.getHello(), is("hello"));
        assertThat(testBean.getWorld(), is("world"));
        assertThat(testBean.getHelloWorld(), is("helloworld" +
                ""));
        assertThat(methodResult, is(true));
        assertThat(result, is(true));
    }
}