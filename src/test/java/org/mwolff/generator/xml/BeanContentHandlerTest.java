/*
 Simple Bean Generator.

 Framework for easy building beans and data objects
 Download: https://gitlab.com/mannewolff/simplebeangenerator

 Copyright (C) 2018 - 2019 Manfred Wolff - Neusta Software Development

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package org.mwolff.generator.xml;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.xml.sax.Attributes;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class BeanContentHandlerTest {

    @Mock
    Attributes atts;

    @Test
    public void testStartElementBean() {
        BeanContentHandler beanContentHandler = new BeanContentHandler();
        Mockito.when(atts.getValue("class")).thenReturn("org.mwolff.generator.data.AdressData");
        Mockito.when(atts.getValue("extends")).thenReturn("org.mwolff.generator.data.GenericData");
        beanContentHandler.startElement("", "bean", "", atts);
        final Bean bean = (Bean) ReflectionTestUtils.getField(beanContentHandler, "bean");
        assertNotNull(bean);
        assertThat(bean.getXclass(), is("org.mwolff.generator.data.AdressData"));
        assertThat(bean.getXextends(), is("org.mwolff.generator.data.GenericData"));
    }

    @Test
    public void testStartElementImportStaticTrue() {
        BeanContentHandler beanContentHandler = new BeanContentHandler();
        Mockito.when(atts.getValue("name")).thenReturn("org.mwolff.generator.data.AdressData");
        Mockito.when(atts.getValue("static")).thenReturn("true");
        ReflectionTestUtils.setField(beanContentHandler, "bean", new Bean());
        beanContentHandler.startElement("", "import", "", atts);
        final Bean bean = (Bean) ReflectionTestUtils.getField(beanContentHandler, "bean");
        assertNotNull(bean);
        final Import ximport = bean.getImports().get(0);
        assertThat(ximport.getName(), is("org.mwolff.generator.data.AdressData"));
        assertThat(ximport.isXstatic(), is(true));
    }

    @Test
    public void testStartElementImportStaticFalse() {
        BeanContentHandler beanContentHandler = new BeanContentHandler();
        Mockito.when(atts.getValue("name")).thenReturn("org.mwolff.generator.data.AdressData");
        Mockito.when(atts.getValue("static")).thenReturn("false");
        ReflectionTestUtils.setField(beanContentHandler, "bean", new Bean());
        beanContentHandler.startElement("", "import", "", atts);
        final Bean bean = (Bean) ReflectionTestUtils.getField(beanContentHandler, "bean");
        assertNotNull(bean);
        final Import ximport = bean.getImports().get(0);
        assertThat(ximport.getName(), is("org.mwolff.generator.data.AdressData"));
        assertThat(ximport.isXstatic(), is(false));
    }

    @Test
    public void testStartElementProperty() {
        BeanContentHandler beanContentHandler = new BeanContentHandler();
        Mockito.when(atts.getValue("name")).thenReturn("street");
        Mockito.when(atts.getValue("type")).thenReturn("String");
        ReflectionTestUtils.setField(beanContentHandler, "bean", new Bean());
        beanContentHandler.startElement("", "property", "", atts);
        final Bean bean = (Bean) ReflectionTestUtils.getField(beanContentHandler, "bean");
        assertNotNull(bean);
        final Property property = bean.getProperties().get(0);
        assertThat(property.getName(), is("street"));
        assertThat(property.getType(), is("String"));
    }

    @Test
    public void testEndElementBean() {
        BeanContentHandler beanContentHandler = new BeanContentHandler();
        ReflectionTestUtils.setField(beanContentHandler, "bean", new Bean());
        beanContentHandler.endElement("", "bean", "");
        final List<Bean> beans = beanContentHandler.getBeans();
        assertThat(beans.size(), is(1));
    }

}