package org.mwolff.test;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.mwolff.generator.config.CommandTransport;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class ConfigurationFactoryTest {

    @Test
    public void getInstance() {
        new ConfigurationFactory();

        CommandTransport transport = ConfigurationFactory.getInstance();
        assertThat(transport, is(notNullValue()));
        assertThat(transport, instanceOf(CommandTransport.class));
    }

    @Test
    public void setDefaults() {
        CommandTransport transport = ConfigurationFactory.getInstance();

        assertThat(transport.getBasepath(), is(notNullValue()));
        assertThat(transport.getBasepath(), is(not(Matchers.isEmptyString())));
        assertThat(transport.getClasspath(), is(notNullValue()));
        assertThat(transport.getClasspath(), is(not(Matchers.isEmptyString())));
        assertThat(transport.getConfigPath(), is(notNullValue()));
        assertThat(transport.getConfigPath(), is(not(Matchers.isEmptyString())));

        assertThat(transport.getProperties(), is(notNullValue()));
        assertThat(transport.getProperties(), is(not(Matchers.isEmptyString())));


    }
}