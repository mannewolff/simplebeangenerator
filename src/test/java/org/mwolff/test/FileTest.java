package org.mwolff.test;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;

import static org.hamcrest.CoreMatchers.is;

public class FileTest {

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Test
    public void testFileFolder () throws  Exception{

        File createdFile= folder.newFile("myfile.txt");
        System.out.println(createdFile.getAbsoluteFile());
        System.out.println(createdFile.getName());
        boolean result = createdFile.exists();
        Assert.assertThat(result, is(true));
        folder.delete();
    }
}
