package org.mwolff.toolbox;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ValidToolsTest {

    ValidTools tools;

    //  List<? extends Object>

    @Test
    public void checkListIsValid() {
        tools = new ValidTools();
        List<String> listToCheck = new ArrayList<>();
        listToCheck.add("Hello World");
        assertThat(true, is(ValidTools.checkListIsValid(listToCheck)));
    }

    @Test
    public void checkListIsNotValidBecauseOfNull() {
        assertThat(false, is(ValidTools.checkListIsValid(null)));
    }

    @Test
    public void checkListIsNotValidBecauseOfEmptyness() {
        List<String> listToCheck = new ArrayList<>();
        assertThat(false, is(ValidTools.checkListIsValid(listToCheck)));
    }
}