/*
 Simple Bean Generator.

 Framework for easy building beans and data objects
 Download: https://gitlab.com/mannewolff/simplebeangenerator

 Copyright (C) 2018 - 2019 Manfred Wolff - Neusta Software Development

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package org.mwolff.toolbox.velocitytools;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

public class VelocityExceptionTest {

    @Test
    public void velocityExceptionDefaultConstructorTest() {
        Assert.assertThat(new VelocityException(), Matchers.notNullValue());
    }

    @Test
    public void velocityExceptionWithMessageAndThrowableTest() {
        final VelocityException velocityException = new VelocityException("message", null);
        Assert.assertThat(velocityException.getMessage(), Matchers.is("message"));
        Assert.assertThat(velocityException.getCause(), Matchers.nullValue());
    }

    @Test
    public void velocityExceptionWithMessageTest() {
        final VelocityException velocityException = new VelocityException("message");
        Assert.assertThat(velocityException.getMessage(), Matchers.is("message"));
    }

    @Test
    public void velocityExceptionWithThrowableTest() {
        final Exception exception = new Exception();
        final VelocityException velocityException = new VelocityException(exception);
        Assert.assertThat(velocityException.getCause(), Matchers.is(exception));
    }

}