/*
 Simple Bean Generator.

 Framework for easy building beans and data objects
 Download: https://gitlab.com/mannewolff/simplebeangenerator

 Copyright (C) 2018 - 2019 Manfred Wolff - Neusta Software Development

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package org.mwolff.toolbox.velocitytools;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.tools.generic.DisplayTool;
import org.junit.Assert;
import org.junit.Test;
import org.mwolff.generator.config.resourceloader.PropertyResourceLoader;
import org.mwolff.generator.xml.Bean;
import org.mwolff.generator.xml.Import;
import org.mwolff.generator.xml.Property;

import java.io.StringWriter;
import java.util.Properties;

import static org.mwolff.generator.config.resourceloader.PropertyResourceLoader.METHOD_CLASSPATH;
//import org.mwolff.toolbox.velocitytools.PropertyLoader.Methods;

public class VelocityMergerTest {

    private static final Logger LOG = LogManager.getLogger(VelocityMergerTest.class);

    @Test
    public void mergeSimpleString() {
        LOG.debug("mergeSimpleString");
        final String s = "We are using $project $name to render this.";
        final VelocityContext context = new VelocityContext();
        context.put("project", "MyProject");
        context.put("name", "MyName");
        final StringWriter w = new StringWriter();
        VelocityMerger.evaluate(context, w, s);
        Assert.assertEquals("We are using MyProject MyName to render this.", w.toString());
    }

    @Test
    public void mergeSimpleKey() throws Exception {
        LOG.debug("mergeSimpleKey");
        final VelocityMerger velocityMerger = new VelocityMerger();
        velocityMerger.setTemplateFileName("simplemerge.vm");
        final PropertyResourceLoader propLoader = new PropertyResourceLoader();
        propLoader.load("example.properties", METHOD_CLASSPATH);
        final Properties properties = propLoader.getProperties();
        velocityMerger.setProperties(properties);
        final String result = velocityMerger.mergeWithPropertyFileWithFilePath("src/test/resources/");
        Assert.assertEquals("Hallo meine Welt ${key} merged.", result);
    }

    @Test
    public void mergeClassPerPropertyfile() throws Exception {
        LOG.debug("mergeClassPerPropertyfile");
        final VelocityMerger velocityMerger = new VelocityMerger();
        velocityMerger.setTemplateFileName("specification.vm");
        final PropertyResourceLoader propLoader = new PropertyResourceLoader();
        propLoader.load("specification.properties", METHOD_CLASSPATH);
        final Properties properties = propLoader.getProperties();
        velocityMerger.setProperties(properties);
        final String result = velocityMerger.mergeWithPropertyWithClassPath();

        final String lineSeparator = System.getProperty("line.separator");

        final String expected = "" + "/**" + lineSeparator + "* Dies ist ein kleiner Test." + lineSeparator + "*"
                + lineSeparator + "* @author: Manfred Wolff" + lineSeparator + "* @version: 1.0" + lineSeparator
                + "*/" + lineSeparator + "public class TestClass  extends Extends implements Implements {"
                + lineSeparator + "}";

        System.out.println("------- expected --------");
        System.out.println(expected);
        System.out.println("------- actual --------");
        System.out.println(result);
        Assert.assertEquals(expected, result);
    }

    @Test
    public void mergeTemplateWithVelocityContext() {
        LOG.debug("mergeTemplateWithVelocityContext");
        final VelocityMerger velocityMerger = new VelocityMerger();
        velocityMerger.setTemplateFileName("specification.vm");
        final VelocityContext context = new VelocityContext();
        context.put("classcomment", "Dies ist ein kleiner Test.");
        context.put("author", "Manfred Wolff");
        context.put("version", "1.0.0");
        context.put("classname", "TestClass");
        final String result = velocityMerger.mergeWithContext(context, "src/test/resources");

        final String lineSeparator = System.getProperty("line.separator");

        final String expected = "" + "/**" + lineSeparator + "* Dies ist ein kleiner Test." + lineSeparator + "*"
                + lineSeparator + "* @author: Manfred Wolff" + lineSeparator + "* @version: 1.0.0" + lineSeparator
                + "*/" + lineSeparator + "public class TestClass {" + lineSeparator + "}";

        Assert.assertEquals(expected, result);
    }

    @Test
    public void mergeWithBean() {
        LOG.debug("mergeWithBean");
        final VelocityMerger velocityMerger = new VelocityMerger();
        velocityMerger.setTemplateFileName("/my-class-template.vm");
        final VelocityContext context = new VelocityContext();

        Property property = new Property();
        property.setName("street");
        property.setType("String");

        Import ximport = new Import();
        ximport.setName("org.mwolff.generator.GenericData");
        ximport.setXstatic(false);


        Bean bean = new Bean();
        bean.addProperty(property);
        bean.addImport(ximport);

        bean.setXclass("Adress");
        bean.setXextends("GenericData");
        context.put("packageString", "org.mwolff.beangenerator");
        context.put("bean", bean);
        context.put("display", new DisplayTool());

        final String result = velocityMerger.mergeWithContext(context, "src/main/resources");
        final String expected = "package org.mwolff.beangenerator;\n" +
                "\n" +
                "import org.mwolff.generator.GenericData;\n" +
                "\n" +
                "/**\n" +
                " * This is a generated class, please don't change. Changes will be overwritten.\n" +
                " * Simple Generator Framework 2.0.2\n" +
                " */\n" +
                "public class Adress {\n" +
                "\n" +
                "    private String street;\n" +
                "\n" +
                "\n" +
                "    public String getStreet() {\n" +
                "        return this.street;\n" +
                "    }\n" +
                "\n" +
                "    public void setStreet(final String street) {\n" +
                "        this.street = street;\n" +
                "    }\n" +
                "}\n";
        Assert.assertEquals(expected, result);
    }
}